<?php

use Illuminate\Http\Request;


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();

});

Route::group(['middleware'=>['auth:api']],function(){
    Route::post('/quote',[
        'uses' => 'QuoteController@save'
    ]);

    Route::get('/quotes',[
        'uses' => 'QuoteController@getQuotes'
    ]);

    Route::get('/quote/{id}',[
        'uses' => 'QuoteController@showQuote'
    ]);

    Route::put('/quote/{id}',[
        'uses' => 'QuoteController@update'
    ]);

    Route::delete('/quote/{id}',[
        'uses' => 'QuoteController@delete'
    ]);

});

    Route::post('/register',['uses'=>'Api\AuthController@register']);
    Route::post('/login',['uses'=>'Api\AuthController@login']);