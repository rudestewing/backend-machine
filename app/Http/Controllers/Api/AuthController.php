<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//load Model
use App\User;

//load Package Laravel 
use Hash;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client as Client;

class AuthController extends Controller
{
    public function register(Request $req)
    {
        $this->validate($req,[
            'email'     => 'required|email|unique:users',
            'name'      => 'required',
            'password'  =>  'required',
        ]);

        $user = new User([
            'name' => $req->name,
            'email' =>$req->email,
            'password' => bcrypt($req->password) 
        ]);
        $user->save();

        $this->flushAccessTokens($user);

        $http = new Client;
        $response = $http->post(url('/oauth/token'),[
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => '2',
                'client_secret' => 'jWuOwdx62utXHQWqdbn7LhBkXdZMbT4Jfd4wfcza',
                'username' => $req->email,
                'password' => $req->password,
                'scope' => '',
            ],
        ]);
        return response()->json([
            'data' => json_decode((string) $response->getBody(),1) 
        ]);
    }

    public function login(Request $req)
    {
        $this->validate($req,[
            'email' => 'required',
            'password' => 'required',
        ]);

        $user = User::where('email',$req->email)->first();
        
        if(!$user){
            return response()->json([
                'status' => 'error',
                'message' => 'user not found !'
            ]);
        }

        $this->flushAccessTokens($user);

        if(Hash::check($req->password,$user->password)){
            $http = new Client;
            $response = $http->post(url('/oauth/token'),[
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => '2',
                    'client_secret' => 'jWuOwdx62utXHQWqdbn7LhBkXdZMbT4Jfd4wfcza',
                    'username' => $req->email,
                    'password' => $req->password,
                    'scope' => '',
                ],
            ]);
            return response()->json(json_decode((string) $response->getBody(),1));
        } else {
            return response()->json([
                'status' => 0,
                'message' => 'invalid credentials' 
            ]);
        }
    }

    private function flushAccessTokens($user)
    {
        if($user->accessTokens()->count() > 0 ){
            $user->accessTokens()->delete();
        }
    } 
}
