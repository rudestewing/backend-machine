<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/* model */
use App\Quote;

class QuoteController extends Controller
{
    public function __construct()
    {

    }

    public function getQuotes()
    {
        $quotes = Quote::all();
        
        $response = [
            'quotes' => $quotes
        ];

        return response()->json($response, 200);
    }

    public function save(Request $req)
    {
        $quote = new Quote;
        $quote->content = $req->content;
        $quote->save();
        
        return response()->json([
            'quote' => $quote
        ],201);
    }

    public function showQuote($id)
    {
        $quote = Quote::find($id);

        return response()->json([
            'quote' => $quote
        ],200);
    }

    public function update(Request $req,$id)
    {
        $quote = Quote::find($id);
        if(!$quote){
            return response()->json([
                'message'=> 'Document Not Found !'
            ],404);

        } else {
            $quote->content = $req->content;
            $quote->save();

            return response()->json([
                'quote' => $quote
            ]);
        }
    }

    public function delete($id)
    {
       $quote = Quote::find($id);

       if(!$quote){
           return response()->json([
               'message' => 'Document Not Found !'
           ],404);
       } else {
           $quote->delete();
           return response()->json([
               'message' => 'Delete Successfull'
           ],200);
       }
    }

}
